## nad_RM6785-userdebug 12 SQ1A.211205.008 eng.ubuntu.20220111.203054 release-keys
- Manufacturer: realme
- Platform: mt6785
- Codename: RM6785
- Brand: realme
- Flavor: nad_RM6785-userdebug
- Release Version: 12
- Id: SQ1A.211205.008
- Incremental: eng.ubuntu.20220111.203054
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: google/coral/coral:12/SQ1A.211205.008/7888514:user/release-keys
- OTA version: 
- Branch: nad_RM6785-userdebug-12-SQ1A.211205.008-eng.ubuntu.20220111.203054-release-keys
- Repo: realme_rm6785_dump


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
